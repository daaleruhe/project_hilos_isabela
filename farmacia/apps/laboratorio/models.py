from django.db import models

# Create your models here.
class Laboratorio(models.Model):
	referencia = models.CharField(max_length=10)
	nombre = models.CharField(max_length=40)

	def __unicode__(self):
		return self.nombre
#agregar direccion